import UIKit

class MessageCellConstant {
    static var smallPadding: CGFloat = 4
    static var normalPadding: CGFloat = 16
    static var mainPadding: CGFloat = 96
}

