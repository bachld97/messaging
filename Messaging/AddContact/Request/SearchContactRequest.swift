struct SearchContactRequest {
    let searchString: String
    
    init(searchString: String) {
        self.searchString = searchString
    }
}
