struct AddContactRequest {
    let contactToAdd: Contact
    
    init(contactToAdd: Contact) {
        self.contactToAdd = contactToAdd
    }
}
