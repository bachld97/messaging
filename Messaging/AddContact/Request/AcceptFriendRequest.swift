struct AcceptFriendRequest {
    let acceptedContact: Contact
    
    init(acceptedContact: Contact) {
        self.acceptedContact = acceptedContact
    }
}

